package com.app.dashcam.data.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Streaming
import timber.log.Timber

interface CameraApi {

    @GET("livesubstream.h264")
    @Streaming
    fun getStream(): Deferred<ResponseBody>

    companion object {
        private val retrofit: Retrofit = Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger
                    { message -> Timber.i(message) }).apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                    .build()
            )
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://192.168.1.1/")
            .build()

        fun getRetrofit(): CameraApi {
            return retrofit.create(CameraApi::class.java)
        }
    }
}