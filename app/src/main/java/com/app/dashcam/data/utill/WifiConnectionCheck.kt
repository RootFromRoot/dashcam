package com.app.dashcam.data.utill

import android.content.Context
import android.net.wifi.SupplicantState
import android.net.wifi.WifiManager
import com.app.dashcam.data.CAMERA_MAC


class WifiConnectionCheck(private val context: Context) {
    private fun getConnectedWifiBssid(): String {
        val wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiManager.connectionInfo

        return if (wifiInfo.supplicantState == SupplicantState.COMPLETED) {
            wifiInfo.bssid
        } else ""
    }

    fun isWifiBssidEqualsCameraBssid() = getConnectedWifiBssid().toLowerCase() == CAMERA_MAC
}