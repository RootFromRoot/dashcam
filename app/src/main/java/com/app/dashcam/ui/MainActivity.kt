package com.app.dashcam.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.app.dashcam.R
import com.app.dashcam.data.utill.WifiConnectionCheck
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    private fun setupView() {
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_carcam ->{}
                R.id.nav_videos ->{}
                R.id.nav_photo_gallery ->{}
                R.id.nav_info ->{}
                R.id.nav_settings ->{}
                R.id.nav_help ->{}
            }
            drawer_layout.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }
        btn_connect.setOnClickListener {
           // if (WifiConnectionCheck(this).isWifiBssidEqualsCameraBssid())
                startActivity(Intent(this, JavaStreamTest::class.java))
          //  else Toast.makeText(this, "You`re not connected to camera WIFI", Toast.LENGTH_SHORT).show()
        }
        tv_how_connect.setOnClickListener { startActivity(Intent(this, HowToConnectActivity::class.java)) }
        startActivity(Intent(this, StreamActivity::class.java))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}
