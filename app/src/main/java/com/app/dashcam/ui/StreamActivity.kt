package com.app.dashcam.ui

import android.graphics.SurfaceTexture
import android.os.Bundle
import android.view.TextureView
import androidx.appcompat.app.AppCompatActivity
import com.app.dashcam.R
import kotlinx.android.synthetic.main.activity_stream.*

class StreamActivity : AppCompatActivity(), TextureView.SurfaceTextureListener {
    private lateinit var encodeDecodeTest: EncodeDecodeTest

    override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture?, p1: Int, p2: Int) {
    }

    override fun onSurfaceTextureUpdated(p0: SurfaceTexture?) {
    }

    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture?): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stream)
        sv.surfaceTextureListener = this

        encodeDecodeTest.doEncodeDecodeVideoFromBuffer()
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {

    }
}