package com.app.dashcam.ui

import android.content.Context
import kotlinx.coroutines.Job
import java.nio.ByteBuffer
import android.os.FileUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jcodec.common.io.IOUtils.readFileToByteArray
import java.io.*
import java.net.URL


class VideoStreamDownloader(private val context: Context) {
    private var stream: InputStream? = null
    private var job: Job? = null
    var tempFile: File
    lateinit var bufferedInputStream: BufferedInputStream
    var onStart: (bufferedInputStream: ByteArray) -> Unit = {}

    init {
        tempFile = File("/sdcard/h264.h264")
        // tempFile.delete()
    }

    companion object {
        private const val TEMP_FILE_NAME = "dashcam.temp"
        private const val TEMP_DIR_NAME = "temp"
    }

    var b = false
    var i = 0
    fun start() {

       //onStart( retrieveByteArrayInputStream(tempFile).readBytes())

         GlobalScope.launch {
             val url = URL("http://192.168.1.1/livesubstream.h264")
             val connection = url.openConnection()
             connection.connect()
             val reader = BufferedInputStream(url.openStream(), 1024)
             reader.use {
                 while (true) {

                     val bytes = ByteArray(1024)

                     it.read(bytes)
                 //    tempFile.appendBytes(bytes)

                   //  if (!b) {
                        onStart(bytes)
                     //   b = true
                   //  }

                     if (i == 2000) {
                         break
                     } else {
                         i++
                     }
                 }
             }
         }
    }

    fun stop() {
        if (tempFile.exists()) {
            tempFile.delete()
        }
    }

    fun retrieveByteArrayInputStream(file: File): ByteArrayInputStream {
        return ByteArrayInputStream(file.readBytes())
    }
}