package com.app.dashcam.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.dashcam.R

class HowToConnectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how_to_connect)
    }

    override fun onNavigateUp(): Boolean {
        onBackPressed()
        return super.onNavigateUp()
    }
}
