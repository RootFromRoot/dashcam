/*
package com.app.dashcam.ui

import android.media.MediaCodec
import android.media.MediaCodec.BufferInfo
import android.R.attr.configure
import android.media.MediaCodecInfo
import android.media.MediaFormat
import android.media.MediaExtractor
import android.view.Surface
import timber.log.Timber


class PlayerThread(private val surface: Surface) : Thread() {
        private var decoder: MediaCodec? = null
    private val MIME_TYPE = "video/avc"
    override fun run() {


                val format = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, 300, 200)
                val mime = format.getString(MediaFormat.KEY_MIME)

                    decoder = MediaCodec.createDecoderByType(mime)
                    decoder!!.configure(format, surface, null, 0)

            if (decoder == null) {
                Timber.e( "Can't find video info!")
                return
            }

            decoder!!.start()

            val inputBuffers = decoder!!.inputBuffers
            var outputBuffers = decoder!!.outputBuffers
            val info = BufferInfo()
            var isEOS = false
            val startMs = System.currentTimeMillis()

            while (!Thread.interrupted()) {
                if (!isEOS) {
                    val inIndex = decoder!!.dequeueInputBuffer(10000)
                    if (inIndex >= 0) {
                        val buffer = inputBuffers[inIndex]
                        val sampleSize = extractor!!.readSampleData(buffer, 0)
                        if (sampleSize < 0) {
                            decoder!!.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
                            isEOS = true
                        } else {
                            decoder!!.queueInputBuffer(inIndex, 0, sampleSize, extractor!!.sampleTime, 0)
                            extractor!!.advance()
                        }
                    }
                }

                val outIndex = decoder!!.dequeueOutputBuffer(info, 10000)
                when (outIndex) {
                    MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED -> { 
                        Timber.d("INFO_OUTPUT_BUFFERS_CHANGED")
                        outputBuffers = decoder!!.outputBuffers
                    }
                    MediaCodec.INFO_OUTPUT_FORMAT_CHANGED ->
                        Timber.d("New format " + decoder!!.outputFormat)
                    MediaCodec.INFO_TRY_AGAIN_LATER ->
                        Timber.d("dequeueOutputBuffer timed out!")
                    else -> {
                        val buffer = outputBuffers[outIndex]
                        Timber.v("We can't use this buffer but render it due to the API limit, $buffer")
                        while (info.presentationTimeUs / 1000 > System.currentTimeMillis() - startMs) {
                            try {
                                Thread.sleep(10)
                            } catch (e: InterruptedException) {
                                e.printStackTrace()
                                break
                            }

                        }
                        decoder!!.releaseOutputBuffer(outIndex, true)
                    }
                }

                // All decoded frames have been rendered, we can stop playing now
                if (info.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
                    Timber.d("OutputBuffer BUFFER_FLAG_END_OF_STREAM")
                    break
                }
            }

            decoder!!.stop()
            decoder!!.release()
            extractor!!.release()
        }
    }
*/
